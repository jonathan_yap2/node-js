//console.log('hello Js')
// console.log('hello world');
//
// var time = 0;
// var timer = setInterval(function(){
//   time +=2
//   console.log(time + ' - bye bye');
//   if (time > 5){
//     clearInterval(timer);
//   }
// },2000);

// console.log(__dirname);
// console.log(__filename);


///function example
//normal function statement

// function sayHi(){
//   console.log('HI');
// };
//
// sayHi();
//
//
//
// // This is an example of a function calling another function.
// // Notice that, the 'fun' is an argument passed into the function then being called.
// function callFunction(fun){
//   fun();
// }
//
// //function expression ; notice the fuction without a name, usually is a lambda function in python
//
// var sayBye=function(){
//   console.log('bye');
// };
//
// // sayBye();
//
// // function call to another function call
// callFunction(sayBye);

/// end of function example

// // basic "import"(in python) in node js
// var stuff = require('./stuff');
// console.log(stuff.counter(['a','b','c']));
// console.log(stuff.adder(1,32));
// console.log(stuff.adder(stuff.pi,5));
// // end of basic "import"(in python) in node js



// // Node Event Emitter example (video 8)
//
// var events = require('events');
// var util = require('util'); //to inherit certain things from other node js object (basically is create inheritance)
//
// // var myEmitter = new events.EventEmitter();
//
// // myEmitter.on('someEvent',function(mssg){
// //   console.log(mssg);
// // });
// //
// // myEmitter.emit('someEvent','the event was emitted');
//
// // a Person "class"
// var Person = function(name){
//   this.name = name;
// };
//
// util.inherits(Person, events.EventEmitter); // inherits EventEmitter to class Person
//
// // instantiate Person class
// var James = new Person('James');
// var James1 = new Person('James1');
// var James2 = new Person('James2');
//
// //store each object into an array
// var people = [James,James1,James2];
//
// // loop the people array, 1 by 1
// // lets see:
// //    - people is an array
// //    - the forEach loop is passing each element in the people's array
// //    - the person is actually an element from people's array
// //    - person is an object of Person
// //    - each object of Person is being attach to events by /*.on*/
// //    - each object can be trigger the events by /*.emit*/
//
// people.forEach(function(person){
//       //console.log('outside of the function call '+ person.name);
//       person.on('speak',function(mssg){
//         console.log(person.name+' said: '+mssg);
//       });
// });
//
// James.emit('speak','hello everyone');
//
// // End Node Event Emitter example

// // files management (video 9)
//
// var fs = require('fs');
//
// // synchronos method
// // var readme = fs.readFileSync('readme.txt','utf8'); // this is a blocking call to read a file.
// // //console.log(readme);
// // fs.writeFileSync('writeme.txt', readme);
// // End synchronos method
//
// // asynchronos method - non blocking
// fs.readFile('readme.txt','utf8',function(err,data){
//   //console.log(data);
//   fs.writeFile('writeme.txt', data);
// });
//
// console.log('test');
// // End asynchronos method - non blocking
//
// // End files management

// create and removing directories (video 10)

//var fs = require('fs');

// unlink is to delete like os.rmdir()(Python)
//fs.unlink('writeme.txt');

// synchronos method
//fs.mkdirSync('stuff');
//fs.rmdirSync('stuff');

// asynchronos method
// fs.mkdir('stuff',function(){
//   fs.readFile('readme.txt','utf8',function(err,data){
//     fs.writeFile('./stuff/writeme.txt',data);
//   });
// });
// fs.unlink('./stuff/writeme.txt',function(){
//   fs.rmdir('stuff');
// });

// End create and removing directories

// // Create server video(12)
// var http = require('http');
//
// var server = http.createServer(function(req,res){
//   console.log('request was made: '+ req.url);
//   res.writeHead(200,{'Content-Type': 'text/plain'});
//   res.end('hello server');
// });
//
// server.listen('3000','127.0.0.1');
// console.log('listening to port 3000');
//
// // End Create Server


// Readable Stream in NodeJs video(14 & 15)
// var http = require('http');
// var fs = require('fs');
//
// var myreadstream = fs.createReadStream(__dirname + '/readme.txt','utf8');
// var mywritestream = fs.createWriteStream(__dirname + '/writeme.txt');
//
// myreadstream.on('data', function(chunk){
//   mywritestream.write(chunk);
//   //console.log('new chunk receive:');
//   //console.log(chunk);
// });

// var server = http.createServer(function(req,res){
//   console.log('request was made: '+ req.url);
//   res.writeHead(200,{'Content-Type': 'text/plain'});
//   res.end('hello server');
// });
//
// server.listen('3000','127.0.0.1');
// console.log('listening to port 3000');

// End Readable Stream in NodeJs




// stream pipes to the client. video(16)

// var http = require('http');
// var fs = require('fs');

// var myreadstream = fs.createReadStream(__dirname + '/readme.txt','utf8');
// var mywritestream = fs.createWriteStream(__dirname + '/writeme.txt');
//
// // there is a short hand to write the read stream and write stream by pipe.
// // myreadstream.on('data', function(chunk){
// //   mywritestream.write(chunk);
// // });
// // there is a short hand to write the read stream and write stream by pipe.

// myreadstream.pipe(mywritestream);

// the 'res' object is a writable stream as well
// var server = http.createServer(function(req,res){
//   console.log('request was made: '+ req.url);
//   res.writeHead(200,{'Content-Type': 'text/plain'});
//
//   var myreadstream = fs.createReadStream(__dirname + '/readme.txt','utf8');
//   myreadstream.pipe(res);
// });
//
// server.listen('3000','127.0.0.1');
// console.log('listening to port 3000');

// End stream pipes to the client.


// Serving HTML Pages video(17)
// var server = http.createServer(function(req,res){
//   console.log('request was made: '+ req.url);
//   res.writeHead(200,{'Content-Type': 'text/html'});
//
//   var myreadstream = fs.createReadStream(__dirname + '/index.html','utf8');
//   myreadstream.pipe(res);
// });
//
// server.listen('3000','127.0.0.1');
// console.log('listening to port 3000');
// End Serving HTML Pages



//Serving JSON Pages video(18)

// var server = http.createServer(function(req,res){
//   console.log('request was made: '+ req.url);
//   res.writeHead(200,{'Content-Type': 'application/json'});
//   var myObj ={
//     name:'j',
//     job:'dev',
//     age:29
//   };
//
//   res.end(JSON.stringify(myObj));
//
// });
//
// server.listen('3000','127.0.0.1');
// console.log('listening to port 3000');

// End Serving JSON Pages

// node js routing video(19)

// var server = http.createServer(function(req,res){
//   console.log('request was made: '+ req.url);
//   if (req.url === '/home' || req.url == '/'){
//       res.writeHead(200,{'Content-Type': 'text/html'});
//       fs.createReadStream(__dirname+'/index.html').pipe(res);
//   }
//   else if (req.url === '/contact'){
//       res.writeHead(200,{'Content-Type': 'text/html'});
//       fs.createReadStream(__dirname+'/contact.html').pipe(res);
//   }
//   else if (req.url === '/api/api01'){
//       var array1 = [{name:'j',age:10},{name:'o',age:20}];
//       res.writeHead(200,{'Content-Type': 'application/json'});
//       res.end(JSON.stringify(array1));
//   }
//   else {
//       res.writeHead(404,{'Content-Type': 'text/html'});
//       fs.createReadStream(__dirname+'/404.html').pipe(res);
//   }
//   // res.writeHead(200,{'Content-Type': 'text/plain'});
//   // res.end('hello world')
// });
//
// server.listen('3000','127.0.0.1');
// console.log('listening to port 3000');

//End node js routing
